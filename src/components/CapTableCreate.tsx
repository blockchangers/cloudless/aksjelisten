import axios from 'axios';
import { ethers } from 'ethers';
import { Box, Button, Grid, Heading, Text, TextInput } from 'grommet';
import React, { useEffect, useState } from 'react';
// import { formatAddress } from '../utils/address';
// import { useHistory } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { SpinnerDiamond } from 'spinners-react';
import Web3Modal from "web3modal";
import { CapTableFactory } from '../contracts/CapTableFactory';
import { OrgData } from '../types/brreg';
import { formatCurrency } from '../utils/numbers';
import { TX_OVERRIDE } from '../utils/web3';
import { getPassword } from '../utils/passwordBlockAPI';


interface Props { }

interface FormData {
    name: string
    symbol: string
    orgNr: string
}

export const CreateCapTable: React.FC<Props> = () => {
    const { handleSubmit, control, formState, errors, watch, setValue } = useForm<FormData>({
        defaultValues: {
            orgNr: "",
            name: "",
            symbol: ""
        }
    })
    const nameWatch = watch("name", "")
    const orgNrWatch = watch("orgNr", "")
    const history = useHistory();
    const [orgData, setOrgData] = useState<OrgData>();
    const [isSearchingBrreg, setIsSearchingBrreg] = useState(false);

    useEffect(() => {
        if (process.env.NODE_ENV === "development") {
            // setValue("orgNr", "915772137")
        }
    }, [setValue])


    useEffect(() => {
        if (formState.touched.symbol) {
            return
        }
        setValue("symbol", nameWatch.substr(0, 4).toUpperCase())
    }, [nameWatch, formState.touched, setValue])

    useEffect(() => {
        const timer = setTimeout(async () => {
            if (orgData && orgNrWatch === orgData.Orgnr.toString()) {
                // This means we have updated with orgData from Brreg
                return
            }
            if (orgNrWatch) {
                setIsSearchingBrreg(true)
                const maybeOrgNr = +orgNrWatch
                setOrgData(undefined)

                const password = getPassword()
                const res = isNaN(maybeOrgNr)
                    ?
                    await axios.get("https://auth-oracle.now.sh/api/v1/org", {
                        params: {
                            name: orgNrWatch,
                            password: password
                        }
                    }).catch(err => {
                        console.log(err.message === "Request failed with status code 401");
                        if (err.message === "Request failed with status code 401") {
                            localStorage.removeItem("brregPassword")
                        }
                        setIsSearchingBrreg(false)
                        console.log("Fant ingen med navn", orgNrWatch);
                        return {
                            data: {
                                result: []
                            }
                        }
                    })
                    :
                    await axios.get("https://auth-oracle.now.sh/api/v1/org", {
                        params: {
                            orgnr: orgNrWatch.replace(/\s/g, ''),
                            password: password
                        }
                    }).catch(err => {
                        setIsSearchingBrreg(false)
                        console.log("Fant ingen med org nummer " + orgNrWatch.replace(/\s/g, ''));
                        return {
                            data: {
                                result: []
                            }
                        }
                    })


                console.log("Found in Brreg => ", res.data.result[0]);
                if (res.data.result[0])
                    setOrgData(res.data.result[0])
                setIsSearchingBrreg(false)
            }
        }, 500)
        return () => {
            clearTimeout(timer)
            setIsSearchingBrreg(false)
        }
    }, [orgData, orgNrWatch, setValue])

    useEffect(() => {
        if (orgData) {
            setValue("name", orgData.Navn)
            setValue("orgNr", orgData.Orgnr.toString(), false)
        }
    }, [orgData, setValue])


    const onSubmit = async (data: FormData) => {
        const web3Modal = new Web3Modal();
        const provider = await web3Modal.connect();
        const web3 = new ethers.providers.Web3Provider(provider as any)
        const signer = web3.getSigner()
        const CONTROLLERS = ["0xC9901c379E672912D86D12Cb8f182cFaf5951940", "0x2B95321E4179821e6B8503cDCf1cb5Ed22366E3B"]
        const CERTIFICATE_SIGNER = "0xC8864Ec7b816EB035b732635392405ab8FCaFC17"
        const capTable = await new CapTableFactory(signer).deploy(
            data.name,
            data.symbol,
            1,
            CONTROLLERS,
            CERTIFICATE_SIGNER,
            false,
            ["0x4100000000000000000000000000000000000000000000000000000000000000"],
            process.env.REACT_APP_QUE ? process.env.REACT_APP_QUE : "please set process.env.REACT_APP_QUE",
            process.env.REACT_APP_ERC1820 ? process.env.REACT_APP_ERC1820 : "please set process.env.REACT_APP_ERC1820",
            ethers.utils.formatBytes32String(data.orgNr),
            TX_OVERRIDE()
        )
        await capTable.deployed()
        history.push("/capTable/" + capTable.address + "/onboard")
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Heading>Opprett aksjeeierbok</Heading>
            <Box gap="medium" margin="large">

                <Box >
                    <Text>Org nr</Text>
                    <Controller as={<TextInput />} name="orgNr" control={control} rules={{ required: true }} placeholder="Skriv selskapets navn eller org nummer for å søke..." />
                    {errors.orgNr && <Text color="red" size="small">* {errors.orgNr.message}</Text>}
                    {isSearchingBrreg &&
                        <Box align="center" >
                            <SpinnerDiamond color="brand" size="50"></SpinnerDiamond>
                            <Text > Søker i Brønnøysundregistrene...</Text>

                        </Box>
                    }

                    {orgData && !isSearchingBrreg &&
                        <Box margin="medium" background="brand" pad="small">
                            <Grid columns={["1/3", "2/3"]} fill="horizontal" gap="small">
                                <Text size="xsmall">Org nr.</Text>
                                <Text size="small" weight="bold">{orgData.Orgnr}</Text>

                                <Text size="xsmall">Navn</Text>
                                <Text size="small" weight="bold">{orgData.Navn}</Text>

                                <Text size="xsmall">Kapital</Text>
                                <Text size="xsmall">{formatCurrency(orgData.Kapital)}</Text>

                                <Text size="xsmall">Aksjer</Text>
                                <Text size="xsmall">{orgData.Kapital / orgData.Aksjer}</Text>

                                <Text size="xsmall">Vedtektsdato</Text>
                                <Text size="xsmall">{orgData.Vedtektsdato}</Text>
                            </Grid>
                        </Box>
                    }

                </Box>

                <Controller as={<TextInput hidden />} name="symbol" control={control} rules={{ required: true }} placeholder="Symbol for selskap..." />
                {errors.symbol && <Text color="red" size="small">* {errors.symbol.message}</Text>}

                <Controller as={<TextInput hidden />} name="name" control={control} rules={{ required: true }} placeholder="Navn på selskap..." />
                {errors.name && <Text color="red" size="small">* {errors.name.message}</Text>}


                <Button
                    type="submit"
                    disabled={formState.isSubmitting /* || Object.keys(formState.touched).length === 0 */}
                    color="brand"
                    label="Opprett aksjeeierbok"
                    margin={{ /* top: "medium" */ }}
                    size="large"
                ></Button>
            </Box>
        </form >
    )
}