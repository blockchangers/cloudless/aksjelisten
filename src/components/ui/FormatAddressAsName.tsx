import Copy from "clipboard-copy";
import { Copy as CopyIcon } from 'grommet-icons';
import { Box, Text, Button } from "grommet";
import React, { useState, useEffect } from "react";
import { getAuthOracle } from "../../utils/contracts";
import { ethers } from "ethers";
interface Props {
  address: string
  copy?: boolean
  size?: string
  notFoundText?: string
}

export const FormatAddressAsName: React.FC<Props> = ({ address, copy = true, size = "small", notFoundText = "Fant ikke enhet" }) => {
  const [color, setColor] = useState<string>("black");
  const [name, setName] = useState("Henter enhet...");

  const addressString = (
    address.substr(0, 5) +
    ".." +
    address.substr(address.length - 2, address.length)
  )

  useEffect(() => {
    let subscribed = true
    const doAsync = async () => {
      if (subscribed) {
        try {
          const authOracle = await getAuthOracle()
          const nameBytes32 = await authOracle.get_name_from_address(address)
          const name = ethers.utils.parseBytes32String(nameBytes32)
          if (name === "")
            throw Error("Not a name")
          setName(name)
        } catch (error) {
          // console.log(error);
          if (notFoundText === "address") {
            setName(addressString)
          } else {
            setName(notFoundText)
          }
        }
      }
    };
    const timeout = setTimeout(doAsync, 200)
    return () => {
      subscribed = false
      clearTimeout(timeout)
    }
  }, [address, addressString, notFoundText])

  const handleCopy = () => {
    setColor("green")
    Copy(address)
    setTimeout(() => {
      setColor("text")
    }, 300)
  }
  return (
    <Box direction="row">
      <Text size={size}>{name}</Text>
      {copy &&
        <Button plain margin={{ left: "5px" }} alignSelf="center" icon={<CopyIcon size="15px" color={color} ></CopyIcon>} onClick={() => handleCopy()} hoverIndicator focusIndicator={false}></Button>
      }
    </Box>
  )
};

