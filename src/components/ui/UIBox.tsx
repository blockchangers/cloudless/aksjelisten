import React from 'react';
import { Box as GrommetBox, Heading } from 'grommet';

interface Props {
    heading?: string
}

export const UIBox: React.FC<Props> = ({ children, heading }) => {

    return (
        <GrommetBox gap="large" alignSelf="start" direction="row-responsive" flex="grow" width="large">
            <GrommetBox gap="small" border pad="medium" round="xsmall" elevation="small" flex="grow">
                {heading &&
                    <Heading level="3">{heading}</Heading>
                }
                {children}
            </GrommetBox>
        </GrommetBox>
    )
}