import axios from 'axios';
import { ethers } from 'ethers';
import { Box, Button, CheckBox, Grid, Text, TextInput } from 'grommet';
import { Actions } from 'grommet-icons';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { PropertyCDPFactory } from "../../contracts/PropertyCDPFactory";
import { OrgData } from '../../types/brreg';
import { getERC20MintableBurnable, useAuthOracle, useCapTable } from '../../utils/contracts';
import { addressToBytes32, NumToHexBytes32 } from '../../utils/dataBytes32Format';
import { Rotate } from '../../utils/Rotate';
import { getWeb3, TX_OVERRIDE } from '../../utils/web3';
import { getPassword } from '../../utils/passwordBlockAPI';

// const DEMO_ADDRESSER = ["Grefsenveien 126", "Sundsveien 10", "Janteigen 4", "Tyristubbveien 11"]
// const DEMO_POSTCODES = ["0141", "5533", "1354", "8412"]
const FIXED_COLLATERAL_HOLDER = "20122819389"

interface Props {
    address: string
}

interface FormData {
    name: string[]
    uuid: string[]
    streetAddress: string[]
    postcode: string[]
    amountShares: string[]
    partition: string[]
    mortgageHolder: string[]
    mortgageHolderName: string[]
    mortgageValue: string[]
    verified: boolean[]
}

interface FormDataSingle {
    name: string
    uuid: string
    streetAddress: string
    postcode: string
    amountShares: string
    partition: string
    mortgageHolder: string
    mortgageHolderName: string
    mortgageValue: string
    verified: boolean
}

export const BatchIssue: React.FC<Props> = ({ address }) => {
    const { handleSubmit, control, errors, watch, setValue, getValues } = useForm<FormData>({
        defaultValues: {
            name: [""],
            uuid: [""],
            streetAddress: [""],
            postcode: ["0"],
            amountShares: ["1"],
            partition: [""],
            mortgageHolder: [],
            mortgageHolderName: [""],
            mortgageValue: [""],
            verified: [false]
        }
    })
    const [capTable] = useCapTable(address)
    const [authOracle] = useAuthOracle()
    const [orgData, setOrgData] = useState<OrgData>();
    const [loading, setLoading] = useState(false);
    const watchUUID = watch("uuid")
    const watchMortgageHolder = watch("mortgageHolder")
    const watchVerified = watch("verified")
    const [searchedUUID, setsearchedUUID] = useState<string[]>([]);
    const [rows, setRows] = useState(1);
    const history = useHistory()

    useEffect(() => {
        if (authOracle) {
            const timer = setTimeout(() => {
                watchUUID.map(async (uuid, index) => {
                    if (uuid !== "" && uuid.length > 2 && searchedUUID.indexOf(uuid) === -1) {
                        const uuidBytes32 = ethers.utils.formatBytes32String(uuid)
                        const ssn = await authOracle.get_name_from_ssn(uuidBytes32)
                        setsearchedUUID(old => [...old, uuid])
                        if (ssn !== "0x0000000000000000000000000000000000000000000000000000000000000000") {
                            setValue(`name[${index}]`, ethers.utils.parseBytes32String(ssn), false)
                            setValue(`verified[${index}]`, true)
                            // setValue(`streetAddress[${index}]`, DEMO_ADDRESSER[index])
                            // setValue(`postcode[${index}]`, DEMO_POSTCODES[index])
                        }
                    }
                })
            }, 1000)
            return () => {
                clearTimeout(timer)
            }
        }
        // console.log(watchUUID);
    }, [authOracle, searchedUUID, setValue, watchUUID])

    useEffect(() => {
        if (authOracle) {
            const timer = setTimeout(() => {
                watchMortgageHolder.map(async (mortgageHolder, index) => {
                    if (mortgageHolder !== "" && mortgageHolder.length > 2 /* && searchedUUID.indexOf(uuid) === -1 */) {
                        const uuidBytes32 = ethers.utils.formatBytes32String(mortgageHolder)
                        const ssn = await authOracle.get_name_from_ssn(uuidBytes32)
                        setsearchedUUID(old => [...old, mortgageHolder])
                        if (ssn !== "0x0000000000000000000000000000000000000000000000000000000000000000") {
                            setValue(`mortgageHolderName[${index}]`, ethers.utils.parseBytes32String(ssn), false)
                        }
                    }
                })
            }, 1000)
            return () => {
                clearTimeout(timer)
            }
        }
        // console.log(watchUUID);
    }, [authOracle, setValue, watchMortgageHolder])

    useEffect(() => {
        const doAsync = async () => {
            if (capTable) {
                setLoading(true)
                const uuidBytes32 = await capTable.getUuid()
                const uuid = ethers.utils.parseBytes32String(uuidBytes32)
                const password = getPassword()
                const res = await axios.get("https://auth-oracle.now.sh/api/v1/org", {
                    params: {
                        orgnr: uuid,
                        password: password
                    }
                }).catch(err => {
                    console.log("Fant ingen med uuid", uuid);
                    return {
                        data: {
                            result: []
                        }
                    }
                }).finally(() => setLoading(false))
                if (res.data.result[0]) {
                    setOrgData(res.data.result[0])
                }
            }
        };
        doAsync();
    }, [capTable])

    // const formData = useMemo(() => (key: keyof FormData, rowNr: number, type: "number" | "text" = "text", defaultValue: string | number = "") => {
    //     return (
    //         <Box >
    //             <Controller as={<TextInput type={type} size="small" /* disabled={lockedFields.indexOf(`${key}[${rowNr}]`) !== -1} */ />} name={`${key}[${rowNr}]`} control={control} rules={{ required: true }} defaultValue={defaultValue} />
    //             {errors[key] && errors[key] + `[${rowNr}]` && <Text color="red" size="small">* {errors[key] + `[${rowNr}]`}</Text>}
    //         </Box>
    //     )
    // }, [control, errors])

    const createArrayWithNumbers = (length: number) => {
        return Array.from({ length }, (_, k) => k);
    }

    const getAddressFromSSNorAddress = async (addressOrSSN: string) => {
        let to = ""
        if (authOracle) {
            try {
                to = addressOrSSN.substr(0, 2) === "0x" ? addressOrSSN : await authOracle.get_address_from_ssn(ethers.utils.formatBytes32String(addressOrSSN))
            } catch (error) {
                console.log("Problem resolving to address when issueShare");
                console.log(error);
            }
        }
        return to
    }

    const issueShare = async (_partition: string, _toAddressOrSSN: string, _amount: string) => {
        if (authOracle && capTable) {
            const partition = ethers.utils.formatBytes32String(_partition)
            const amount = ethers.utils.parseEther(_amount)
            const txData = "0x11"
            console.log(_toAddressOrSSN);

            const to = await getAddressFromSSNorAddress(_toAddressOrSSN)
            console.log(to);

            const capTableContract = await capTable.issueByPartition(partition, to, amount, txData, TX_OVERRIDE())
            await capTableContract.wait()
        }
    }

    const createMortgageOnBehalf = async (_partition: string, _toAddressOrSSN: string, _amount: string, _mortgageHolder: string, _mortgageValue: string) => {
        if (authOracle && capTable) {
            const partition = ethers.utils.formatBytes32String(_partition)
            const amount = ethers.utils.parseEther(_amount)
            const to = await getAddressFromSSNorAddress(_toAddressOrSSN)
            const mortgageHolder = await getAddressFromSSNorAddress(_mortgageHolder)
            const mortgageValue = parseInt(_mortgageValue)
            const signer = await getWeb3()
            const propertyCDP = await new PropertyCDPFactory(signer).deploy(
                process.env.REACT_APP_ERC1820 || "set env var",
                TX_OVERRIDE()
            )
            await propertyCDP.deployed()

            const transferIssueByPartition = await capTable.issueByPartition(partition, propertyCDP.address, amount, "0x4341505f5441424c455f4344505f464c41470000000000000000000000000000" +
                NumToHexBytes32(mortgageValue) +
                addressToBytes32(mortgageHolder) +
                addressToBytes32(to), TX_OVERRIDE())
            await transferIssueByPartition.wait();
            // const transferByPartition = await capTable.transferByPartition(
            //     partition,
            //     propertyCDP.address,
            //     amount,
            //     "0x4341505f5441424c455f4344505f464c41470000000000000000000000000000" +
            //     NumToHexBytes32(mortgageValue) +
            //     addressToBytes32(mortgageHolder) +
            //     addressToBytes32(to)
            // );
            // await transferByPartition.wait();

            const cTokenAddress = await propertyCDP.getCToken();
            const pTokenAddress = await propertyCDP.getPToken();

            const cToken = await getERC20MintableBurnable(cTokenAddress)
            const pToken = await getERC20MintableBurnable(pTokenAddress)

            console.log("cTokenBalance", (await cToken.totalSupply()).toString());
            console.log("pTokenBalance", (await pToken.totalSupply()).toString());

            // const tx1 = await cToken.transfer(to, amount);
            // await tx1.wait();

            // console.log("TRANSFERING pTokens to", mortgageHolder);

            // const tx2 = await pToken.transfer(mortgageHolder, mortgageValue);
            // await tx2.wait();

        }
    }

    const verify = async (rowNr: number) => {
        if (authOracle) {
            const ssnText = getValues(`uuid[${rowNr}]`) as string
            const nameText = getValues(`name[${rowNr}]`) as string
            const ssnBytes32 = ethers.utils.formatBytes32String(ssnText)
            const nameBytes32 = ethers.utils.formatBytes32String(nameText)
            const suggest_ssn_to_address = await authOracle.suggest_ssn_to_address(ssnBytes32, nameBytes32, TX_OVERRIDE())
            await suggest_ssn_to_address.wait()
            setsearchedUUID(old => old.filter(value => value !== ssnText))
            setValue(`ssn[${rowNr}]`, ssnText, false)
        }
    }

    const onSubmit = async (data: FormData) => {
        console.log("onSubmit=>", data);
        // ISSUE SHARES
        const contractPromises = createArrayWithNumbers(rows)
            .map(async rowNr => {
                console.log("Doing ROW nr", rowNr);

                if (parseInt(data.mortgageValue[rowNr]) > 0 && data.mortgageHolder[rowNr]) {
                    // const signer = await getWeb3()
                    // await issueShare(data.partition[rowNr], await signer.getAddress(), data.amountShares[rowNr])
                    await createMortgageOnBehalf(data.partition[rowNr], data.uuid[rowNr], data.amountShares[rowNr], /* TODO : data.mortgageHolder[rowNr] */ FIXED_COLLATERAL_HOLDER, data.mortgageValue[rowNr])
                } else {
                    await issueShare(data.partition[rowNr], data.uuid[rowNr], data.amountShares[rowNr])
                }
                return rowNr
            })
        await Promise.all(contractPromises)
        history.push("/captable/" + address)
    }

    const tableColoumns = ["small", "small", "xsmall", "small", "xsmall", "xsmall", "xsmall", "small", "xsmall", "xsmall",]
    return (
        <Box gap="small">
            <Box pad="small" background="brand" align="start">
                {loading &&
                    <Text size="small">Henter selskaps data... <Rotate><Actions></Actions></Rotate></Text>
                }
                {orgData &&
                    <Text size="small">Selskapet har {orgData.Kapital / orgData.Aksjer} utstedt aksjer som må registreres på fysiske personer eller juridiske enheter.</Text>
                }
            </Box>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Box elevation="large" gap="small" pad="small" margin={{ top: "large" }}>
                    <Grid columns={tableColoumns} fill="horizontal" gap="small">
                        <Text size="small" weight="bold" truncate>Fnr. eller Orgnr.</Text>
                        <Text size="small" weight="bold" truncate>Navn</Text>
                        <Text size="small" weight="bold" truncate>Verify</Text>
                        <Text size="small" weight="bold" truncate>Addresse</Text>
                        <Text size="small" weight="bold" truncate>Postnummer</Text>
                        <Text size="small" weight="bold" truncate>Antall aksjer</Text>
                        <Text size="small" weight="bold" truncate>Aksjeklasse</Text>
                        <Text size="small" weight="bold" truncate>Panthaver</Text>
                        <Text size="small" weight="bold" truncate>Panth. navn</Text>
                        <Text size="small" weight="bold" truncate>Pantverdi</Text>

                    </Grid>
                    {createArrayWithNumbers(rows).map((rowNr, i, arr) =>
                        <Grid columns={tableColoumns} fill="horizontal" gap="small" key={rowNr}>

                            <Box >
                                <Controller as={<TextInput size="small" />} name={`uuid[${rowNr}]`} control={control} rules={{ required: true }} defaultValue={""} disabled={watchVerified[rowNr]} />
                                {errors["uuid"] && errors["uuid"][rowNr] && <Text color="red" size="xsmall">* {errors["uuid"][rowNr].type}</Text>}
                            </Box>

                            <Box >
                                <Controller as={<TextInput size="small" />} name={`name[${rowNr}]`} control={control} rules={{ required: true }} defaultValue={""} disabled={watchVerified[rowNr]} />
                                {errors["name"] && errors["name"][rowNr] && <Text color="red" size="xsmall">* {errors["name"][rowNr].type}</Text>}
                            </Box>

                            <Box justify="center" margin="small" >
                                <Controller as={<CheckBox />} name={`verified[${rowNr}]`} control={control} rules={{ required: true }} defaultValue={false} disabled={watchVerified[rowNr]} onChange={() => verify(rowNr)} />
                                {errors["verified"] && errors["verified"][rowNr] && <Text color="red" size="xsmall">* {errors["verified"][rowNr].type}</Text>}
                            </Box>


                            <Box >
                                <Controller as={<TextInput size="small" />} name={`streetAddress[${rowNr}]`} control={control} rules={{ required: false }} defaultValue={""} disabled={false} />
                                {errors["streetAddress"] && errors["streetAddress"][rowNr] && <Text color="red" size="xsmall">* {errors["streetAddress"][rowNr].type}</Text>}
                            </Box>

                            <Box >
                                <Controller as={<TextInput size="small" type="number" />} name={`postcode[${rowNr}]`} control={control} rules={{ required: false, /* minLength: 4 */ }} defaultValue={"0"} disabled={false} />
                                {errors["postcode"] && errors["postcode"][rowNr] && <Text color="red" size="xsmall">* {errors["postcode"][rowNr].type}</Text>}
                            </Box>

                            <Box >
                                <Controller as={<TextInput size="small" type="number" />} name={`amountShares[${rowNr}]`} control={control} rules={{ required: true, min: 1 }} defaultValue={"0"} disabled={!watchVerified[rowNr]} />
                                {errors["amountShares"] && errors["amountShares"][rowNr] && <Text color="red" size="xsmall">* {errors["amountShares"][rowNr].type}</Text>}
                            </Box>

                            <Box >
                                <Controller as={<TextInput size="small" />} name={`partition[${rowNr}]`} control={control} rules={{ required: true }} defaultValue={""} disabled={!watchVerified[rowNr]} />
                                {errors["partition"] && errors["partition"][rowNr] && <Text color="red" size="xsmall">* {errors["partition"][rowNr].type}</Text>}
                            </Box>

                            <Box >
                                <Controller as={<TextInput size="small" />} name={`mortgageHolder[${rowNr}]`} control={control} rules={{ required: false }} defaultValue={""} disabled={!watchVerified[rowNr]} />
                                {errors["mortgageHolder"] && errors["mortgageHolder"][rowNr] && <Text color="red" size="xsmall">* {errors["mortgageHolder"][rowNr].type}</Text>}
                            </Box>

                            <Box >
                                <Controller as={<TextInput size="small" />} name={`mortgageHolderName[${rowNr}]`} control={control} rules={{ required: false }} defaultValue={""} disabled={!watchVerified[rowNr]} />
                                {errors["mortgageHolderName"] && errors["mortgageHolderName"][rowNr] && <Text color="red" size="xsmall">* {errors["mortgageHolderName"][rowNr].type}</Text>}
                            </Box>

                            <Box >
                                <Controller as={<TextInput size="small" type="number" />} name={`mortgageValue[${rowNr}]`} control={control} rules={{ required: false }} defaultValue={"0"} disabled={!watchVerified[rowNr]} />
                                {errors["mortgageValue"] && errors["mortgageValue"][rowNr] && <Text color="red" size="xsmall">* {errors["mortgageValue"][rowNr].type}</Text>}
                            </Box>

                        </Grid>
                    )}
                    <Box gap="large" alignSelf="end" direction="row-responsive" align="end">
                        <Button color="black" label="Legg til ny rad" onClick={() => setRows(rows + 1)} style={{ borderRadius: "0px" }}></Button>
                        <Button color="red" label="Fjern nederste rad" onClick={() => setRows(rows - 1)} disabled={rows === 1} style={{ borderRadius: "0px" }}></Button>
                        <Button color="black" label="Utsted aksjer" type="submit" /* disabled={!formState.isValid || formState.isSubmitting} */ style={{ borderRadius: "0px" }}></Button>
                    </Box>
                </Box>
            </form>

        </Box>

    )
}