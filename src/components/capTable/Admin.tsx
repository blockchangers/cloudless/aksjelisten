import React, { useEffect, useState } from 'react';
import { Box, Text, Grid, TextInput, Button } from 'grommet';
import { CapTable } from '../../contracts/CapTable';
import { getWeb3, TX_OVERRIDE } from '../../utils/web3';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { FormatAddress } from '../ui/FormatAddress';
import { ethers } from 'ethers';

interface Props {
    address: string
}

interface CapTableData {
    controllers: string[]
}

export const Admin: React.FC<Props> = ({ address }) => {

    const [account, setAccount] = useState<string>();
    const [capTable, setCapTable] = useState<CapTable>();
    const [data, setData] = useState<CapTableData>();
    const [newBoardDirector, setNewBoardDirector] = useState("");


    // Get CapTable
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTable(new CapTableFactory(signer).attach(address));
            setAccount(await signer.getAddress())
        };
        doAsync();
    }, [address])

    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && account) {

                const controllers = await capTable.controllers()

                setData({ controllers });
            }
        };
        doAsync();
    }, [capTable, address, account])

    const [newOperator, setNewOperator] = useState<string>("");

    const handleAddController = async () => {
        if (capTable) {
            console.log(newOperator);

            const authorizeOperator = await capTable.authorizeOperator(newOperator, TX_OVERRIDE())
            await authorizeOperator.wait()

            setNewOperator("")
        }
    }
    const handleNewBoardDirector = async () => {
        if (capTable) {
            console.log(newBoardDirector);
            try {
                ethers.utils.getAddress(newBoardDirector)
                const transferOwnership = await capTable.transferOwnership(newBoardDirector, TX_OVERRIDE())
                await transferOwnership.wait()

                setNewBoardDirector("")
            } catch (error) {
                console.log(error);

            }
        }
    }

    return (
        <Box gap="medium">
            {address &&
                <Grid columns={["small", "flex"]}>
                    <Text truncate>Addresse</Text>
                    <FormatAddress address={address} />
                </Grid>
            }
            {address &&
                <Grid columns={["small", "flex"]}>
                    <Text>Legg til operatør</Text>
                    <Box gap="small">
                        <TextInput size="small" value={newOperator} onChange={(e) => setNewOperator(e.target.value)}></TextInput>
                        <Button label="Legg til" disabled={newOperator === ""} onClick={() => handleAddController()}></Button>
                    </Box>
                    <Box></Box>
                    <Text size="xsmall">Operatører brukes dersom aksjeeierboken skal kunne opereres på av esterne enheter eller systemer. Feks. om Oslo Børs skal kjøpe og selge aksjer i aksjeeierboken, kan disse legges til som en tillitsfull operatør.</Text>
                </Grid>
            }
            {address &&
                <Grid columns={["small", "flex"]}>
                    <Text>Sett ny styreleder</Text>
                    <Box gap="small">
                        <TextInput size="small" value={newBoardDirector} onChange={(e) => setNewBoardDirector(e.target.value)}></TextInput>
                        <Button label="Sett ny" disabled={newBoardDirector === ""} onClick={() => handleNewBoardDirector()}></Button>
                    </Box>
                </Grid>
            }
            {data &&
                <Grid columns={["small", "flex"]}>
                    <Text >Kontrollere</Text>
                    <Box gap="small">
                        {data.controllers.map((adr, i) => (
                            <Box key={i} background="grey" pad="small" round>
                                <FormatAddress address={adr}></FormatAddress>
                            </Box>
                        ))}
                    </Box>
                </Grid>
            }
        </Box>
    )
}