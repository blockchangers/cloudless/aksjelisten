import React, { useEffect, useState } from 'react';
import { Box, Text, Grid } from 'grommet';
import { CapTable } from '../../contracts/CapTable';
import { getWeb3 } from '../../utils/web3';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { ethers } from 'ethers';
import { FormatAddress } from '../ui/FormatAddress';

interface Props {
    address: string
}

export interface CapTableData {
    address: string
    symbol: string
    totalSupply: string
    controllers: string[]
    isMinter: boolean
    isOwner: boolean
    defaultPartitions: string[]
}

export const InfoAdvanced: React.FC<Props> = ({ address }) => {

    const [account, setAccount] = useState<string>();
    const [capTable, setCapTable] = useState<CapTable>();
    const [data, setData] = useState<CapTableData>();


    // Get CapTable
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTable(new CapTableFactory(signer).attach(address));
            setAccount(await signer.getAddress())
        };
        doAsync();
    }, [address])

    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && account) {
                const symbol = await capTable.symbol().catch(() => "No symbol found");
                const totalSupplyBN = await capTable
                    .totalSupply()
                    .catch(() => ethers.constants.Zero);
                const totalSupply = ethers.utils.formatEther(totalSupplyBN);
                const controllers = await capTable.controllers()
                const isMinter = await capTable.isMinter(account)
                const isOwner = await capTable.isOwner()
                const defaultPartitions = await capTable.getDefaultPartitions()
                setData({ address, symbol, totalSupply, controllers, isMinter, isOwner, defaultPartitions });
            }
        };
        doAsync();
    }, [capTable, address, account])

    const canOverride = (address: string) => {
        if (data) {
            if (data.controllers.find(adr => adr === account)) {
                return true
            }
            if (data.isOwner) {
                return true
            }
        }

        return false
    }

    return (
        <Box gap="small">
            {address &&
                <Grid columns={["small", "flex"]}>
                    <Text>Ethereum addresse</Text>
                    <Text weight="bold"><FormatAddress address={address}></FormatAddress></Text>
                </Grid>
            }
            {data &&
                <Grid columns={["small", "flex"]}>
                    <Text>Rettigheter</Text>
                    <Text></Text>
                </Grid>
            }
            {data && account &&
                <Grid columns={["small", "flex"]}>
                    <Text >Kan overstyre</Text>
                    <Text weight="bold">{canOverride(account) ? "Ja" : "Nei"}</Text>
                </Grid>
            }
            {data &&
                <Grid columns={["small", "flex"]}>
                    <Text >Kan utstede</Text>
                    <Text weight="bold">{data.isMinter ? "Ja" : "Nei"}</Text>
                </Grid>
            }
        </Box>
    )
}