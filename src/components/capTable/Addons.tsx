import { Box, Grid } from 'grommet';
import React from 'react';
import { Multisig } from './addons/Multisig';
import { TX_OVERRIDE } from '../../utils/web3';
import { useCapTable } from '../../utils/contracts';

interface Props {
    address: string
}

export const Addons: React.FC<Props> = ({ address }) => {
    const [capTable] = useCapTable(address)

    const handleMultisigCreated = async (address: string) => {
        if (capTable) {
            const transferOwnership = await capTable.transferOwnership(address, TX_OVERRIDE())
            await transferOwnership.wait()
        }
    }

    return (
        <Box margin="small">
            <Grid>
                <Multisig handleCreated={(address) => handleMultisigCreated(address)}></Multisig>

            </Grid>
        </Box>
    )
}