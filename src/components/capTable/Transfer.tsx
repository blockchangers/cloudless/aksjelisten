import React, { useEffect, useState } from 'react';
import { getWeb3, TX_OVERRIDE } from '../../utils/web3';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { CapTable } from '../../contracts/CapTable';
import { TextInput, Box, Button, Select } from 'grommet';
import { ethers } from 'ethers';

interface Props {
    address: string
}

export const Transfer: React.FC<Props> = ({ address }) => {
    const [capTable, setCapTable] = useState<CapTable>();
    const [account, setAccount] = useState<string>();

    // Get CapTable
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTable(new CapTableFactory(signer).attach(address));
            setAccount(await signer.getAddress())
        };
        doAsync();
    }, [address])

    const [myPartitions, setMyPartitions] = useState<string[]>([]);
    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && account) {
                const partitionsBytes32 = await capTable.partitionsOf(account).catch((): string[] => [])
                const partitions = partitionsBytes32.map(bytes32 => ethers.utils.parseBytes32String(bytes32))
                setMyPartitions(partitions)
            }
        };
        doAsync();
    }, [address, capTable, account])

    const [to, setTo] = useState<string>(process.env.NODE_ENV === "development" ? "0xC9901c379E672912D86D12Cb8f182cFaf5951940" : "");
    const [amount, setAmount] = useState<number>();
    const [partition, setPartition] = useState<string>();

    // Hydrate development data
    useEffect(() => {
        if (process.env.NODE_ENV === "development") {
            setTo("0xFa48Bd6EaA82759ffBe8b25439c5E8f65E4CdF21")
            setAmount(2)
            setPartition(myPartitions[0])
        }
    }, [myPartitions])

    const transfer = async () => {
        if (capTable && to && amount && partition) {
            const tx = await capTable.transferByPartition(ethers.utils.formatBytes32String(partition), to, ethers.utils.parseEther(amount.toString()), "0x11", TX_OVERRIDE())
            await tx.wait()
        }
    }

    return (
        <>
            <Box margin="small" direction="row" gap="small">
                <Box basis="100%" margin="small">
                    <TextInput
                        style={{ minWidth: "100%" }}
                        placeholder="Til addresse"
                        value={to}
                        onChange={event => setTo(event.target.value)}
                    />
                </Box>

            </Box>
            <Box margin="small" direction="row" gap="small">
                <Box basis="2/3" margin="small">
                    <Select
                        placeholder="Aksjeklasse"
                        options={myPartitions}
                        value={partition}
                        onChange={({ option }) => setPartition(option)}
                    />
                </Box>
                <Box basis="1/3" margin="small">
                    <TextInput
                        placeholder="Antall aksjer"
                        value={amount}
                        type="number"
                        onChange={event => setAmount(parseInt(event.target.value))}
                    />
                </Box>
            </Box>

            <Box margin="small">
                <Button label="Overfør aksjer" onClick={() => transfer()}></Button>
            </Box>
        </>
    )

}