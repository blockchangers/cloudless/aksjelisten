import { Box, DataTable } from 'grommet';
import React, { useState, useEffect } from 'react';
import { useCapTable, getMultisig } from '../../utils/contracts';
import { contractImplementsInterface } from '../../utils/hookHelpers';
import { FormatAddressAsName } from '../ui/FormatAddressAsName';

interface Props {
    address: string,
    tokenHolderList: string[]
}

interface Account {
    address: string
    isMultisig: boolean
    owners: string[]
    roles: string[]
}

export const Roles: React.FC<Props> = ({ address, tokenHolderList }) => {
    const [capTable] = useCapTable(address)
    // const [pageOffset, /* setTokenHoldersOffset */] = useState(0);
    // const [pageLimit, /* setTokenHolderPageLimit */] = useState(10);
    // const [/* currentAccount */, setCurrentAccount] = useState("");
    const [accounts, setAccounts] = useState<Account[]>();

    useEffect(() => {
        const doAsync = async () => {
            if (capTable && address && tokenHolderList) {
                const owner = await capTable.owner()
                const ownerAccount = await getAccount(owner, ["Styrelder"])
                const tokenHolderAccounts = await Promise.all(tokenHolderList.map(async (address) => {
                    return getAccount(address)
                }))
                const controllers = await capTable.controllers()
                const controllerAccounts = await Promise.all(controllers.map(async (address) => {
                    return getAccount(address, ["Kontroller"])
                }))
                setAccounts([ownerAccount, ...tokenHolderAccounts, ...controllerAccounts])
            }
        };
        doAsync();
    }, [address, capTable, tokenHolderList])

    useEffect(() => {
        console.log(accounts);

    }, [accounts])

    const getAccount = async (address: string, roles = ["Eier"]) => {

        const isMultisig = await contractImplementsInterface(address, "ERC1400Multisig")
        let multisigOwners: string[] = []
        if (isMultisig) {
            const multisig = await getMultisig(address)
            multisigOwners = await multisig.getOwners()
        }
        return {
            address: address,
            isMultisig: isMultisig,
            owners: multisigOwners,
            roles: roles,
        }

    }

    return (
        <Box>
            <DataTable
                data={accounts}
                columns={[
                    {
                        property: 'address',
                        header: "Enhet / person / gruppe",
                        primary: true,
                        render: (data) => <FormatAddressAsName address={data.address} notFoundText="address"></FormatAddressAsName>
                    },
                    {
                        property: 'roles',
                        header: "Rolle",
                        render: (data) => data.roles.join(",")
                    },
                    // {
                    //     property: 'isMultisig',
                    //     header: "Delegert",
                    //     render: (data) => {
                    //         return data.isMultisig ? "Ja" : "Nei"
                    //     }
                    // },
                    {
                        property: 'owners',
                        header: "Delegert til",
                        render: (data: Account) => data.owners.map(address => <FormatAddressAsName address={address} notFoundText="address"></FormatAddressAsName>)
                    }
                ]}
            >

            </DataTable>
        </Box>
    )
}