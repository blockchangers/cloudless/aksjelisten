import { Box, Button, Grid, Text, TextInput } from 'grommet';
import { Group } from 'grommet-icons';
import React, { useEffect, useState } from 'react';
import { MultiSigWalletFactory } from "../../../contracts/MultiSigWalletFactory";
import { getWeb3 } from '../../../utils/web3';
import { FormatAddress } from '../../ui/FormatAddress';

interface Props {
    handleCreated?: (address: string) => void
}

export const Multisig: React.FC<Props> = ({ handleCreated }) => {
    const [multisigOwners, setMultisigOwners] = useState<string[]>([]);
    const [newMultisigOwner, setNewMultisigOwner] = useState("");
    const [signaturesRequired, setSignaturesRequired] = useState<number>(0);

    const [currentAccount, setCurrentAccount] = useState<string>();

    useEffect(() => {
        const doAsync = async () => {
            const web3 = await getWeb3()
            const currentAddress = await web3.getAddress()
            setCurrentAccount(currentAddress)
        };
        doAsync();
    }, [])

    const createMultisig = async () => {
        if (multisigOwners.length < 1) {
            alert("Må legge til eiere først.")
            return
        }
        const web3 = await getWeb3()
        const currentAddress = await web3.getAddress()
        if (signaturesRequired > multisigOwners.length) {
            return alert("Signatur krav må være lavere en antall addresser")
        }
        const multisig = await new MultiSigWalletFactory(web3).deploy([currentAddress, ...multisigOwners], signaturesRequired)
        await multisig.deployed()
        if (handleCreated) {
            handleCreated(multisig.address)
        }
    }

    const columns = () => {
        return ["auto", "small"]
    }

    useEffect(() => {
        console.log(newMultisigOwner);

    }, [newMultisigOwner])
    return (
        <Box background="grey" gap="small" >
            <Grid columns={columns()} margin={{ bottom: "small" }}  >
                <Text weight="bold" alignSelf="start">Delegert eierskap</Text>
                <Box align="end"><Group></Group></Box>
            </Grid>
            <Grid columns={columns()} gap="small">
                <TextInput placeholder="Legg til addresser her" onBlur={e => setNewMultisigOwner(e.target.value)}></TextInput>
                <Button label="Legg til eier" onClick={(e) => {
                    if (newMultisigOwner) {
                        setMultisigOwners((old) => [...old, newMultisigOwner])
                        setNewMultisigOwner("")
                    }
                }}
                    style={{ borderRadius: "0px" }}></Button>
                <TextInput placeholder="Sett antall signaturer" type="number" value={signaturesRequired} onChange={e => {
                    setSignaturesRequired(parseInt(e.target.value))
                }}></TextInput>
            </Grid>
            {multisigOwners.length > 0 &&
                <Box pad="small" margin="small" elevation="small">
                    {currentAccount &&
                        <Box gap="small" >
                            <Text>Fremtidige eiere:</Text>
                            <Box direction="row">
                                <FormatAddress address={currentAccount}></FormatAddress>
                                <Text>(din addresse)</Text>
                            </Box>
                        </Box>
                    }
                    {multisigOwners.map(address => (
                        <Box key={address}>
                            <FormatAddress address={address}></FormatAddress>
                        </Box>
                    ))}
                </Box>
            }

            <Button label="Opprett delegert eierskap" style={{ borderRadius: "0px" }} onClick={() => createMultisig()} disabled={multisigOwners.length === 0 || signaturesRequired === 0}></Button>
        </Box>

    )
}