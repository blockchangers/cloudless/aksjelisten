import { ethers } from 'ethers';
import { BigNumber } from 'ethers/utils';
import { Box, DataTable, Heading, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { SpinnerDiamond } from 'spinners-react';
import { CapTable } from '../../contracts/CapTable';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { filterCDPHolders, getColleralDetails } from '../../utils/cdpHelpers';
import { formatBN } from '../../utils/numbers';
import { getWeb3 } from '../../utils/web3';
import { FormatAddressAsName } from '../ui/FormatAddressAsName';

interface Props {
    address: string,
    tokenHolderList: string[]
}

interface CollateralBalance {
    address: string,
    partition: string
    amount: BigNumber
    collateralHolders?: string[]
    collateralAmounts?: BigNumber[]
}




export const CollateralBalances: React.FC<Props> = ({ address, tokenHolderList }) => {
    const [capTable, setCapTable] = useState<CapTable>();
    const [/* currentAccount */, setAccount] = useState("");
    const [pageOffset, /* setTokenHoldersOffset */] = useState(0);
    const [pageLimit, /* setTokenHolderPageLimit */] = useState(10);
    const [partitions, setPartitions] = useState<string[]>([]);

    const [collateralBalances, setCollateralBalance] = useState<CollateralBalance[]>([]);

    // Get CapTable and Partitions
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            const capTable = new CapTableFactory(signer).attach(address)
            let partitionsBytes32 = await capTable.totalPartitions()
            setCapTable(capTable);
            setAccount(await signer.getAddress())
            setPartitions(partitionsBytes32)
        };
        doAsync();
    }, [address])



    // get CollateralBalances
    useEffect(() => {
        const doAsync = async () => {
            console.log("RUNNING", !capTable, partitions.length > 0, tokenHolderList.length > 0);

            if (capTable && partitions.length > 0 && tokenHolderList.length > 0) {

                console.log("tokenHolderList", tokenHolderList);
                // CONSIDER - If race errors make this for loop instead. 
                // let collateralBalanceHoldersArray = []
                // for (const tokenHolder of tokenHolderList) {
                const promises = tokenHolderList.reverse().slice(pageOffset, Math.min(tokenHolderList.length, pageOffset + pageLimit)).map(async tokenHolder => {
                    console.log("TokenHolder", tokenHolder);
                    console.log("partitions", partitions);

                    const tokenHolderByPartitionsPromises = await partitions.map(async (partitionBytes32) => {
                        const balance = await capTable.balanceOfByPartition(partitionBytes32, tokenHolder)
                        return {
                            address: tokenHolder,
                            amount: balance,
                            partition: partitionBytes32
                        }
                    })
                    const tokenHolderByPartitions = await Promise.all(tokenHolderByPartitionsPromises)
                    const cdpHolderAddresses = await filterCDPHolders(tokenHolderByPartitions.map(tokenHolderByPartition => tokenHolderByPartition.address))

                    // FOR LATER IN CODE --- If tokenHolde is not CDP, we know it does not hold collateral so we can add it as a collateralBalanceHolder without collateral
                    const balanceTokenHolders = tokenHolderByPartitions.filter(tokenHolderByPartition => {
                        if (cdpHolderAddresses.indexOf(tokenHolderByPartition.address) === -1) {
                            return true
                        }
                        return false
                    })
                    const cdpTokenHolders = tokenHolderByPartitions.filter(tokenHolderByPartition => {
                        if (cdpHolderAddresses.indexOf(tokenHolderByPartition.address) !== -1) {
                            return true
                        }
                        return false
                    })

                    // Lets start getting collateral info
                    const collateralDetails = await getColleralDetails(cdpHolderAddresses).catch(err => {
                        console.log("error in coll", err)
                        throw Error(err)
                    })
                    console.log("collateralDetails", collateralDetails);

                    const cdpTokenHoldersExpanded = cdpTokenHolders.map(cdpTokenHolder => {
                        return collateralDetails[cdpTokenHolder.address].cToken.map(cToken => {
                            return {
                                address: cToken.holder,
                                amount: cdpTokenHolder.amount,
                                partition: cdpTokenHolder.partition,
                                collateralHolders: collateralDetails[cdpTokenHolder.address].pToken.map(pToken => pToken.holder),
                                collateralAmounts: collateralDetails[cdpTokenHolder.address].pToken.map(pToken => pToken.balance),
                            }
                        })
                    }).flat()

                    // collateralBalanceHoldersArray.push()
                    return [...balanceTokenHolders, ...cdpTokenHoldersExpanded]
                }
                )

                const collateralBalanceHoldersArray = await Promise.all(promises)
                const collateralBalanceHolders = collateralBalanceHoldersArray.flat().map((v, i) => ({ ...v, id: i }))
                console.log("collateralBalanceHolders", collateralBalanceHolders);

                setCollateralBalance(collateralBalanceHolders)
            }
        };
        doAsync();
    }, [capTable, pageLimit, pageOffset, partitions, tokenHolderList])


    return (

        <Box>
            {collateralBalances.length > 0 ?
                <DataTable
                    data={collateralBalances}
                    columns={[
                        {
                            property: 'id',
                            header: "#",
                            primary: true,
                            render: () => ""
                        },
                        {
                            property: 'address',
                            header: <Text>Person / enhet</Text>,
                            render: (data: CollateralBalance) => (
                                <Box gap="small" pad="small" >
                                    <FormatAddressAsName address={data.address}></FormatAddressAsName>
                                </Box>

                            )
                        },
                        {
                            property: 'amount',
                            header: <Text>Antall</Text>,
                            render: (data: CollateralBalance) => (
                                <Box gap="small" pad="small" >
                                    <Text truncate>{formatBN(data.amount)}</Text>
                                </Box>
                            )
                        },
                        {
                            property: 'partition',
                            header: <Text>Aksjeklasse</Text>,
                            render: (data: CollateralBalance) => (
                                <Box gap="small" pad="small" >
                                    <Text truncate>{ethers.utils.parseBytes32String(data.partition)}</Text>
                                </Box>
                            )
                        },
                        {
                            property: 'collateralHolders',
                            header: <Text>Panthaver</Text>,
                            render: (data: CollateralBalance) => (

                                <Box gap="small" pad="small" >
                                    {data.collateralHolders &&
                                        data.collateralHolders.map((collateralHolder, i) => (
                                            <Box key={i}>
                                                <FormatAddressAsName address={collateralHolder}></FormatAddressAsName>
                                            </Box>
                                        ))
                                    }
                                </Box>
                            )
                        },
                        {
                            property: 'collateralAmounts',
                            header: <Text>Pantsum</Text>,
                            render: (data: CollateralBalance) => (
                                <Box gap="small" pad="small" >
                                    {data.collateralAmounts &&
                                        <Text truncate>{data.collateralAmounts.map(bn => bn.toString()).join(",")}</Text>
                                    }
                                </Box>
                            )
                        }
                    ]}
                ></DataTable>
                : <Box align="center" >
                    <SpinnerDiamond color="brand"></SpinnerDiamond>
                    <Heading level={4}>Henter pant og balanser... </Heading>
                </Box>
            }
        </Box>

    )
}