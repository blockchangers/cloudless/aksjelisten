import React from 'react';
import BRREG_LOGO_SVG from './../assets/brreg_logo.svg'
import BRREG_LOGO_SMALL_PNG from './../assets/brreg_logo.png'
import { Header, Button, Box, Image, ResponsiveContext, Heading } from 'grommet';
import {
    Link
} from "react-router-dom";
interface Props { }

export const Naviagation: React.FC<Props> = () => {
    const size = React.useContext(ResponsiveContext);
    return (
        <Header background="white" alignContent="between">
            <Box direction="row" align="center">
                <Link to="/">
                    {size === "small"
                        ? <Image src={BRREG_LOGO_SMALL_PNG} margin="small" height="37px"></Image>
                        : <Image src={BRREG_LOGO_SVG} margin="small" height="37px"></Image>
                    }
                </Link>

                <Box alignContent="end" justify="end" pad={{ top: "8px" }} style={{ color: "#010202", fontWeight: "lighter" }} >
                    <Heading level={4} size="small">Forvalt</Heading>
                </Box>
            </Box>

            <Box margin={{ right: "small" }} direction="row" gap="medium">
                <Link to="/captable/create">
                    <Button size="small" label="Opprett" hoverIndicator focusIndicator={false} />
                </Link>
                <Link to="/que/list">
                    <Button size="small" label="Kø" hoverIndicator focusIndicator={false} />
                </Link>
                <Link to="/register/list">
                    <Button size="small" label="Register" hoverIndicator focusIndicator={false} />
                </Link>

            </Box>
        </Header>
    )
}