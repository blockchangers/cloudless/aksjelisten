import { Box, Button, Grid, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useAuthOracle, useERC1820 } from '../utils/contracts';
import { getWeb3 } from '../utils/web3';
import { FormatAddressAsName } from './ui/FormatAddressAsName';
import { Multisig } from './capTable/addons/Multisig';
import { ethers } from 'ethers';
import { getInterfaceImplementer } from '../utils/hookHelpers';

interface Props { }

export const Account: React.FC<Props> = () => {
    const [account, setAccount] = useState<string>();
    const [authOracle] = useAuthOracle()
    const [erc1820] = useERC1820()
    const [multisigAddress, setMultisigAddress] = useState("");


    // get name
    useEffect(() => {
        const doAsync = async () => {
            const web3 = await getWeb3()
            const address = await web3.getAddress()
            setAccount(address)
        };
        doAsync();
    }, [account, authOracle])

    // check for delegated account
    useEffect(() => {
        const doAsync = async () => {
            if (account) {
                const [implementer, error] = await getInterfaceImplementer(account, "ERC1400Multisig")
                if (!error) {
                    setMultisigAddress(implementer)
                }
            }
        };
        doAsync();
    }, [account, authOracle])


    const coloumns = () => {
        return ["small", "auto"]
    }

    const handleCreatedMultisig = async (address: string) => {
        if (!erc1820) {
            return alert("Fant ikke erc1820")
        }
        if (!account) {
            return alert("Fant ikke account")
        }
        const ERC1820_ACCEPT_MULTISIG = ethers.utils.solidityKeccak256(
            ["string"],
            ["ERC1400Multisig"]
        );
        const setInterfaceImplementer = await erc1820.setInterfaceImplementer(account, ERC1820_ACCEPT_MULTISIG, address)
        await setInterfaceImplementer.wait()
    }

    return (
        <Box gap="medium">
            {account &&
                <Grid columns={coloumns()} gap="small" >
                    <Text>Enhet</Text>
                    <FormatAddressAsName address={account} />
                </Grid>
            }
            {account &&
                <Grid columns={coloumns()} gap="small" >
                    <Text>Lag delegert konto</Text>
                    <Multisig handleCreated={(address) => handleCreatedMultisig(address)}></Multisig>
                </Grid>
            }
            {!account &&
                <Grid columns={coloumns()} gap="small" >
                    <Text>Wallet</Text>
                    <Button style={{ borderRadius: "0px" }} label="Skaff Metamask" onClick={e => { window.location.href = "https://metamask.io/download.html" }} plain size="small"></Button>
                </Grid>
            }
            {multisigAddress &&
                <Grid columns={coloumns()} gap="small" >
                    <Text>Delt eierskap</Text>
                    <Text>Ja</Text>
                </Grid>
            }

        </Box>
    )
}