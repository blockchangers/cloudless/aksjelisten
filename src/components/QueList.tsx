/* eslint-disable react-hooks/exhaustive-deps */
import { ethers, Signer } from 'ethers';
import { Box, DataTable, Heading, RadioButtonGroup, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import { SpinnerDiamond } from 'spinners-react';
import { CapTableFactory } from '../contracts/CapTableFactory';
import { CapTableQue } from '../contracts/CapTableQue';
import { CapTableQueFactory } from '../contracts/CapTableQueFactory';
import { getWeb3 } from '../utils/web3';
import { formatBN } from '../utils/numbers';
import { BigNumber } from 'ethers/utils';


interface Props { }

export const QueList: React.FC<Props> = () => {
    const [capTableQue, setCapTableQue] = useState<CapTableQue>();
    const [data, setData] = useState<{ address: string, name: string, totalSupply: BigNumber }[]>();
    const history = useHistory();
    const [pageOffset, /* setListOffset */] = useState(0);
    const [pageLimit, /* setpageSize */] = useState(10);
    const [listType, setListType] = useState("qued");

    const setList = async (list: string[]) => {
        const signer = await getWeb3();
        const dataPromises = list.reverse().slice(pageOffset, Math.min(list.length, pageOffset + pageLimit)).map(address => capTableData(address, signer))
        const data = await Promise.all(dataPromises)
        setData(data)
    }

    const capTableData = async (address: string, signer: Signer) => {
        const capTable = new CapTableFactory(signer).attach(address);
        const name = await capTable.name().catch(() => "No company found");
        // const symbol = await capTable.symbol().catch(() => "No symbol found");
        const totalSupplyBN = await capTable
            .totalSupply()
            .catch(() => ethers.constants.Zero);
        return { address, name, totalSupply: totalSupplyBN };
    };

    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTableQue(new CapTableQueFactory(signer).attach(process.env.REACT_APP_QUE || "0x6d20198614407a4d3268F189b6eeF91f09c72149"))
        }
        doAsync()
        return () => {
            setCapTableQue(undefined)
        }
    }, []);

    const updateList = async (type: "qued" | "approved" | "declined" | string) => {
        console.log("updateList call", type);
        if (capTableQue) {
            if (type === "qued")
                setList(await capTableQue.listQued().catch(() => []))
            else if (type === "approved")
                setList(await capTableQue.listApproved().catch(() => []))
            else if (type === "declined")
                setList(await capTableQue.listDeclined().catch(() => []))
            else
                setList([])
        }
    }

    // Initial list fetch
    useEffect(() => {
        if (capTableQue) {
            updateList("qued")
        }
    }, [capTableQue])

    // Initial list fetch
    useEffect(() => {
        if (listType) {
            setList([])
            updateList(listType)
        }
    }, [listType])


    return (
        <>
            <Heading>Aksjeeierbok kø</Heading>
            <Box direction="row" margin="medium">
                <RadioButtonGroup
                    direction="row"
                    name="doc"
                    options={[{
                        value: 'qued',
                        label: 'Kø'
                    }, {
                        value: 'approved',
                        label: 'Godkjent'
                    }, {
                        value: 'declined',
                        label: 'Avslått'
                    }]}
                    value={listType}
                    onChange={(event) => setListType(event.target.value)}
                />
            </Box>
            {!data ?
                <Box align="center">
                    <SpinnerDiamond color="brand"></SpinnerDiamond>
                    <Text>Henter aksjeeierbøker </Text>
                </Box>
                :
                data.length > 0
                    ?
                    <DataTable
                        onClickRow={(event: any) => {
                            history.push("/captable/" + event.datum.address);
                        }}
                        columns={[
                            {
                                property: 'name',
                                header: <Text>Name</Text>,
                                "search": true,
                            },
                            {
                                property: 'totalSupply',
                                header: <Text>Aksjer</Text>,
                                align: "center",
                                render: (data) => formatBN(data.totalSupply)

                            },

                        ]}
                        data={data}
                    />
                    :
                    <Text>Tom liste</Text>

            }

        </>
    )
}