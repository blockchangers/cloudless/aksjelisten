import React, { useEffect } from 'react';
import { Grommet, Box, Heading, Grid, Text } from 'grommet';
import { Home } from "./pages/Home";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { CreateCapTable } from './components/CapTableCreate';
import { QueList } from './components/QueList';
import { CapTabelPage } from './pages/CapTabelPage';
import { RegistryList } from './components/RegistryList';
import { OnBoardCompanyPage } from './pages/OnBoardCompanyPage';
import { Theme } from './utils/theme';
import { Naviagation } from './components/Navigation';
import { AccountPage } from './pages/AccountPage';


function App() {

  // LOG THINGS
  useEffect(() => {

    console.log("Velkommen til aksjelisten, her er litt testinfo");
    console.log("#### 1 ####");
    console.log("Husk å være pålogget Skyfri noden");
    console.log("https://e0mg3k95c6:hiK58VGAHlSpxnpiVTFtJGDtuaGZOIdQRVkDfAErulA@e0fsgog2j5-e0h0jkl669-rpc.de0-aws.kaleido.io");
    console.log("#### 2 ####");
    console.log("Trenger du en BRREG kontroller");
    console.log("PUB: 0xC9901c379E672912D86D12Cb8f182cFaf5951940");
    console.log("PRIV: 7a3bbccd03c17f110e59514fb4bec8a1c1565876abfe97f8e5d6f5ef31fa6955");
    console.log("#### 3 ####");
    console.log("Sertifiseringer er slått av, men kan skrus på isåfall er certifikator satt til");
    console.log("PUB: 0xC8864Ec7b816EB035b732635392405ab8FCaFC17");
    console.log("PRIV: e66ad73bdeba7c1d6b9ec5da4490da48132d6587c94bed2aed8fdb2df9a7d487");
    console.log("#### 4 ####");
    console.log("Aksjeeierbok køen er nå på: ", process.env.REACT_APP_QUE);
    console.log("Aksjeeierbok registeret er nå på: ", process.env.REACT_APP_REGISTRY);
    console.log("#### END ####");
    console.log(`
    owner
      ssn: "17035318933",
    seller
      ssn: "20060342955",
    propertyManager
      ssn: "25061640072",
    bank
      ssn: "28031932191",
    broker
      ssn: "04117807199",
    controller
      ssn: "05084817521",
    authOracle
      ssn: "01110851509",
    buyer
      ssn: "29038146902",
    OBOS:
      ssn: "20122819389"
    OBOS Banken:
      ssn: "13072937796"
    `);

  }, [])

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Grommet theme={Theme} full>
        <Box background="white" fill="vertical">
          <Grid
            rows={["auto", "flex", "small"]}
            columns={["full"]}
            areas={[
              { name: "header", start: [0, 0], end: [1, 0] },
              { name: "main", start: [0, 1], end: [1, 1] },
              { name: "footer", start: [0, 2], end: [1, 2] }
            ]}
          >
            <Box gridArea="header" background="background-back">
              <Naviagation></Naviagation>
            </Box>

            <Box gridArea="main" background="background-back" align="center" height={{ min: "large" }}>

              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/capTable/create">
                  <CreateCapTable />
                </Route>
                <Route path="/capTable/:address/onboard">
                  <OnBoardCompanyPage></OnBoardCompanyPage>
                </Route>
                <Route path="/capTable/:address">
                  <CapTabelPage></CapTabelPage>
                </Route>
                <Route path="/que/list">
                  <QueList></QueList>
                </Route>
                <Route path="/register/list">
                  <RegistryList></RegistryList>
                </Route>
                <Route path="/account/me">
                  <AccountPage />
                </Route>
              </Switch>

            </Box>

            <Box gridArea="footer" background="background-back">
              <Box margin={{ top: "large" }} background="brand" align="center" justify="center" alignContent="center" fill="horizontal" style={{ minHeight: "200px" }} gap="large">
                <Heading level="4">Laget av Brønnøysundregistrene</Heading>
                <Link to="/account/me">
                  <Text style={{ color: "white" }}>Konto</Text>
                </Link>
              </Box>
            </Box>

          </Grid>
        </Box>
      </Grommet>
    </Router>
  );
}

export default App;
