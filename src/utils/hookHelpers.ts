import { ethers } from "ethers";
import { getWeb3 } from "./web3";
import { getERC1820 } from "./contracts";

export const contractImplementsInterface = async (
  _address: string,
  _interface: string
) => {
  const abi = [
    "function canImplementInterfaceForAddress(bytes32,address) external view returns (bytes32)",
  ];
  const ERC1820_ACCEPT_MAGIC = ethers.utils.solidityKeccak256(
    ["string"],
    ["ERC1820_ACCEPT_MAGIC"]
  );
  const ERC1400_MULTISIG = ethers.utils.solidityKeccak256(
    ["string"],
    [_interface]
  );
  let implementsInterface = false;
  try {
    const signer = await getWeb3();
    const contract = new ethers.Contract(_address, abi, signer);
    const bytesAcceptMagicMaybe = await contract.canImplementInterfaceForAddress(
      ERC1400_MULTISIG,
      ethers.constants.AddressZero
    );
    if (bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC) {
      implementsInterface = true;
    }
  } catch (error) {
    console.log(error);
  }
  return implementsInterface;
};

export const getInterfaceImplementer = async (
  _address: string,
  _interface: string
): Promise<[string, boolean]> => {
  const formattedInterface = ethers.utils.solidityKeccak256(
    ["string"],
    [_interface]
  );
  try {
    const erc1820 = await getERC1820();
    const implementer = await erc1820.getInterfaceImplementer(
      _address,
      formattedInterface
    );

    if (implementer !== ethers.constants.AddressZero) {
      return [implementer, false];
    }
  } catch (error) {
    console.log(error);
  }
  return [ethers.constants.AddressZero, true];
};
