import Web3Modal, { IProviderOptions } from "web3modal";
import { ethers } from "ethers";
import { JsonRpcSigner, Web3Provider } from "ethers/providers";

export const getWeb3 = async (): Promise<JsonRpcSigner> => {
  const web3ModalProvider = await getNativeProvider();
  const web3provider = new ethers.providers.Web3Provider(web3ModalProvider as Web3Provider);
  return await web3provider.getSigner();
};

export const getNativeProvider = async () => {
  const providerOptions: IProviderOptions = {};
  const web3Modal = new Web3Modal({
    // network: "mainnet",
    cacheProvider: true,
    providerOptions, // required
  });
  return await web3Modal.connect();
};

export const fallbackProvider = () => {
  const provider = new ethers.providers.JsonRpcProvider(
    "https://e0fsgog2j5-e0h0jkl669-rpc.de0-aws.kaleido.io" //"https://e0fsgog2j5-e0h0jkl669-rpc.de0-aws.kaleido.io"
  );
  return ethers.Wallet.createRandom().connect(provider);
};

export const TX_OVERRIDE = () => {
  return {
    gasPrice: ethers.utils.parseUnits("0.0", "gwei"),
    gasLimit: 7999999,
  };
};
