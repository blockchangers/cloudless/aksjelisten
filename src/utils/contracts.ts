import { AuthOracleFactory } from "./../contracts/AuthOracleFactory";
import { AuthOracle } from "./../contracts/AuthOracle";
import { getWeb3 } from "./web3";
import { useState, useEffect } from "react";
import { CapTableFactory } from "../contracts/CapTableFactory";
import { CapTable } from "../contracts/CapTable";
import { PropertyCDPFactory } from "../contracts/PropertyCDPFactory";
import { ERC20MintableBurnableFactory } from "../contracts/ERC20MintableBurnableFactory";
import { MultiSigWalletFactory } from "../contracts/MultiSigWalletFactory";
import { ERC1820RegistryFactory } from "../contracts/ERC1820RegistryFactory";
import { ERC1820Registry } from "../contracts/ERC1820Registry";

// export const AuthOracle = (provider: any) => {
//   return AuthOracleFactory.connect(
//     process.env.REACT_APP_AUTH_ORACLE || "set env var",
//     provider
//   );
// };
export const getMultisig = async (address: string) => {
  return MultiSigWalletFactory.connect(address, await getWeb3());
};

export const getERC20MintableBurnable = async (address: string) => {
  return ERC20MintableBurnableFactory.connect(address, await getWeb3());
};

export const getPropertyCDP = async (address: string) => {
  return PropertyCDPFactory.connect(address, await getWeb3());
};

export const getAuthOracle = async () => {
  return AuthOracleFactory.connect(
    process.env.REACT_APP_AUTH_ORACLE || "set env var",
    await getWeb3()
  );
};
export const getERC1820 = async () => {
  return ERC1820RegistryFactory.connect(
    process.env.REACT_APP_ERC1820 || "set env var",
    await getWeb3()
  );
};

export const getCapTable = async (capTableAddress: string) => {
  return CapTableFactory.connect(capTableAddress, await getWeb3());
};

export const useCapTable = (
  capTableAddress: string
): [CapTable | undefined, boolean, string] => {
  const [contract, setContract] = useState<CapTable>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    getCapTable(capTableAddress)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        setContract(_contract);
      });
  }, [capTableAddress, error]);
  return [contract, loading, error];
};

export const useAuthOracle = (): [AuthOracle | undefined, boolean, string] => {
  const [contract, setContract] = useState<AuthOracle>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    getAuthOracle()
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        setContract(_contract);
      });
  }, [error]);
  return [contract, loading, error];
};

export const useERC1820 = (): [
  ERC1820Registry | undefined,
  boolean,
  string
] => {
  const [contract, setContract] = useState<ERC1820Registry>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    getERC1820()
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        setContract(_contract);
      });
  }, [error]);
  return [contract, loading, error];
};

const handleError = (error: any, handler: (err: string) => void) => {
  if (error) {
    if (error.hasOwnProperty("message")) {
      handler(error.message);
    } else {
      handler(error);
    }
  } else {
    handler("Udefined error");
  }
  return undefined;
};
