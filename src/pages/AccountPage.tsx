import React from 'react';

import { Account } from '../components/Account';
import { Box, Heading } from 'grommet';

interface Props { }

export const AccountPage: React.FC<Props> = () => {

    return (
        <Box >
            <Heading alignSelf="center" level={3}>Konto</Heading>
            <Account></Account>
        </Box>
    )
}