import { Box, Button, Grid, Heading, Layer } from 'grommet';
import { ChapterAdd, InProgress, Resources, Transaction } from 'grommet-icons';
import React, { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { SpinnerDiamond } from 'spinners-react';
import { Addons } from '../components/capTable/Addons';
import { Admin } from '../components/capTable/Admin';
import { CapTableHeading } from '../components/capTable/CapTableHeading';
import { CollateralBalances } from '../components/capTable/CollateralBalances';
import { Info } from '../components/capTable/Info';
import { InfoAdvanced } from '../components/capTable/InfoAdvanced';
import { Issue } from '../components/capTable/Issue';
import { Roles } from '../components/capTable/Roles';
import { Transfer } from '../components/capTable/Transfer';
import { QueDetails } from '../components/que/QueDetails';
import { UIBox } from '../components/ui/UIBox';
import { getERC1400Addresses } from '../utils/cdpHelpers';
interface Props { }

export const CapTabelPage: React.FC<Props> = () => {
    const { address } = useParams<{ address: string }>();

    const [tokenHolderList, setTokenHolderList] = useState<string[]>();
    const [loadingTokenHolderList, setLoadingTokenHolderList] = useState(false);

    const [show, setShow] = useState<boolean | string>("");
    const [showCollateralBalances, setShowCollateralBalances] = useState(false);
    const [showRoles, setshowRoles] = useState(true);

    const getTokenholderList = useCallback(async () => {
        if (address && !tokenHolderList) {
            setLoadingTokenHolderList(true)
            const tokenHolders = await getERC1400Addresses(address)
            setLoadingTokenHolderList(false)
            setTokenHolderList(tokenHolders)
        }
    }, [address, tokenHolderList]);

    // Get tokenHolderList at render
    useEffect(() => {
        setTimeout(getTokenholderList, 1000)
    }, [getTokenholderList])

    return (
        <>
            <CapTableHeading address={address}></CapTableHeading>
            {address &&
                <Box gap="large" >

                    <UIBox>
                        <Grid columns={["1/2", "1/2"]} fill="horizontal">
                            <Heading level="3">Informasjon</Heading>
                            <Heading level="3">Avansert</Heading>
                            <Info address={address}></Info>
                            <InfoAdvanced address={address}></InfoAdvanced>
                        </Grid>
                    </UIBox>

                    <UIBox heading="Handlinger">
                        <Grid gap="small" columns={{ size: "small", count: 3 }}>

                            <Button
                                icon={<Transaction />}
                                label="Overføre"
                                onClick={() => setShow("transfer")}
                                style={{ borderRadius: "0px" }}
                            />

                            <Button
                                icon={<ChapterAdd />}
                                label="Utestede"
                                onClick={() => setShow("issue")}
                                style={{ borderRadius: "0px" }}
                            />

                            <Button
                                icon={<InProgress />}
                                label="Kø"
                                onClick={() => setShow("que")}
                                style={{ borderRadius: "0px" }}
                            />

                            <Button
                                icon={<InProgress />}
                                label="Administrasjon"
                                onClick={() => setShow("admin")}
                                style={{ borderRadius: "0px" }}
                            />
                            <Button
                                icon={<Resources />}
                                label="Utvidelser"
                                onClick={() => setShow("addons")}
                                style={{ borderRadius: "0px" }}
                            />


                        </Grid>
                    </UIBox>


                    <UIBox heading="Roller">
                        {showRoles && tokenHolderList && tokenHolderList.length > 0 &&
                            <Roles address={address} tokenHolderList={tokenHolderList}></Roles>

                        }
                        {showRoles && loadingTokenHolderList &&
                            <Box align="center" >
                                <SpinnerDiamond color="brand"></SpinnerDiamond>
                                <Heading level={4}>Henter eiere... </Heading>
                            </Box>
                        }
                        {(!showRoles || !tokenHolderList) &&
                            <Box>
                                <Button label="Hent roller og rettigheter" onClick={() => {
                                    getTokenholderList()
                                    setshowRoles(true)
                                }} style={{ borderRadius: "0px" }}
                                    disabled={loadingTokenHolderList}></Button>
                            </Box>
                        }
                    </UIBox>


                    <UIBox heading="Aksjeierbok">
                        {showCollateralBalances && tokenHolderList && tokenHolderList.length > 0 &&
                            < CollateralBalances address={address} tokenHolderList={tokenHolderList}></CollateralBalances>

                        }
                        {showCollateralBalances && loadingTokenHolderList &&
                            <Box align="center" >
                                <SpinnerDiamond color="brand"></SpinnerDiamond>
                                <Heading level={4}>Henter pant og balanser... </Heading>
                            </Box>
                        }
                        {(!showCollateralBalances || !tokenHolderList) &&
                            <Box>
                                <Button label="Hent aksjeeierboken"
                                    onClick={() => {
                                        getTokenholderList()
                                        setShowCollateralBalances(true)
                                    }}
                                    style={{ borderRadius: "0px" }}
                                    disabled={loadingTokenHolderList}></Button>
                            </Box>
                        }
                    </UIBox>

                    {show &&
                        <Layer
                            onEsc={() => setShow(false)}
                            onClickOutside={() => setShow(false)}
                            modal

                        >
                            <Box margin="large" >

                                {show === "transfer" &&
                                    <Transfer address={address}></Transfer>
                                }
                                {show === "issue" &&
                                    <Issue address={address}></Issue>
                                }
                                {show === "que" &&
                                    <QueDetails address={address}></QueDetails>
                                }
                                {show === "admin" &&
                                    <Admin address={address}></Admin>
                                }
                                {show === "addons" &&
                                    <Addons address={address}></Addons>
                                }
                                <Button margin="small" label="Lukk" onClick={() => setShow(false)} style={{ borderRadius: "0px" }} />
                            </Box>
                        </Layer>
                    }
                </Box>
            }
        </>
    )
}