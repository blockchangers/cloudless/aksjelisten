export interface OrgData {
    Aksjer: number
    Kapital: number
    Navn: string
    Orgnr: number
    Vedtektsdato: string
}
